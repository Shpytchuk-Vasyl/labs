type User = {
  _id?: string;
  firstName: string;
  lastName: string;
  group: string;
  gender: "F" | "M";
  birthday: string;
  status: boolean;
  unreadMessages?: [chatId: string, unread: number][];
};

type UnreadMessages = {
  userId: string;
  unread: string; // [chatId: string, unread: number][] this to json
};


type Message = {
  _id?: string;
  chatId: string;
  author: User;
  createdAt: string;
  updatedAt: string;
  text: string;
};

type Chat = {
  _id?: string;
  name: string;
  messages: Message[];
  users: User[];
};


