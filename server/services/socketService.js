import { UserService } from "./userService.js";

class _SocketService {
  wss = null;

  init(wss) {
    this.wss = wss;
    this.wss.on("connection", (ws) => {
      //console.log("Client connected");
      ws.on("login", async (message) => {
        //console.log(`Received message => ${message}`);
        const user = await UserService.authUser(await JSON.parse(message));
        if (!user) {
          ws.disconnect();
          
        } else {
          ws.user = user;
        }
      });
    });
  }

  sendMsgToUsers(typeMsg, msg, usersId = []) {
    let arr = Array.from(usersId);
    //console.log("sendMsgToUsers", typeMsg, msg, usersId);
    //console.log("Clients count", this.wss.sockets.sockets);
    this.wss.sockets.sockets.forEach((client) => {
      if (usersId.find(id => client.user._id.toString() == id.toString())) {
       // console.log("send to user", client.user._id);
        client.emit(typeMsg, msg);
        arr = arr.filter((id) => id.toString() != client.user._id.toString());
      }
    });
    //console.log("sendMsgToUsers afte arr", arr);
    return arr;
  }

  sendMsgAllUsers(typeMsg, msg) {
    this.wss.sockets.sockets.forEach((client) => {
        client.emit(typeMsg, msg);
    });
  }

}

export const SocketService = new _SocketService();
