import { UserService } from "../../services/userService.js";
import { MyError, sendErrorMsg, tryCathc } from "../../utils/index.js";
import { Router } from "express";
const router = Router();

router.get("/logout", async (req, res) => {
  try {
    UserService.updateStatusAndAvatar(req.session.userId, false);
    req.session.destroy();
    sendErrorMsg(res, 200, "Successful exit");
  } catch (err) {
    console.log(err);
    if (err instanceof MyError) {
      sendErrorMsg(res, err.code, err.message);
      //console.log("send MyError:", err.code, err.message);
    } else sendErrorMsg(res, 500, "Something went wrong");
  }
});

router.get("/login", async (req, res) => {
  //console.log("req.session", req.session.userId);
  try {
    if (req.session.userId) {
      const user = await UserService.getUser(req.session.userId);
      res.json({ user: user, role: req.session.role});
    } else throw new MyError("Entry is prohibiten", 401);
  } catch (err) {
    //console.log(err);
    if (err instanceof MyError) {
      sendErrorMsg(res, err.code, err.message);
      //console.log("send MyError:", err.code, err.message);
    } else sendErrorMsg(res, 500, "Something went wrong");
  }
});

router.post("/login", async (req, res) => {
  //console.log("req.body", req.body);
  try {
    const user = await UserService.authUser(req.body);
    if (!user) throw new MyError("Entry is prohibited", 401);
    UserService.updateStatusAndAvatar(user._id, true, req.body.avatar);
    user.avatar = req.body.avatar;
    req.session.userId = user._id;
    const role = { ...user._doc }.role || "STUDENT";
    req.session.role = role;
    req.session.save();
    res.json({ user: user, role: role });
  } catch (err) {
    //console.log(err);
    if (err instanceof MyError) {
      sendErrorMsg(res, err.code, err.message);
      //console.log("send MyError:", err.code, err.message);
    } else sendErrorMsg(res, 500, "Something went wrong");
  }
});

router.post("/save", async (req, res) => {
  try {
    UserService.updateUserUnreadMsg(
      req.session.userId,
      req.body.map(({ chatId, unread }) => {
        return { chatId, unread };
      }) || []
    );
  } catch (err) {
    //console.log(err);
  }
});

export default router;
