import { MyError, tryCathc } from "../../utils/index.js";
import { Router } from "express";
import { SocketService } from "../../services/socketService.js";
import { TaskService } from "../../services/taskServise.js";
const router = Router();

router.get("/", async (req, res) => {
  await tryCathc(res, async () => {
    return {
      stages: ["TO DO", "IN PROGRESS", "DONE"],
      tasks: await TaskService.getAllTask() || []
    }
  });
});

router.post("/", async (req, res) => {
  let task = null;
  await tryCathc(res, async () => {
    if (req.session.role != "ADMIN")
      throw new MyError("You are can't create task", 403);
    task = await TaskService.createTask(req.body);
    return task;
  });
  if (task) {
    SocketService.sendMsgAllUsers("new-task", task);
  }
});

router.put("/", async (req, res) => {
  let task = null;
  await tryCathc(res, async () => {
    return task = await TaskService.updateTask(req.body.id, req.body.task);
  });
  if (task) {
    SocketService.sendMsgAllUsers("update-task", task);
  }
});

export default router;
