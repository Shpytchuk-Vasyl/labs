import { config } from "dotenv";
import express from "express";
import http from "http";
import session from "express-session";
import cors from "cors";
import bodyParser from "body-parser";

import MongoStore from "connect-mongo";

import { db } from "./mongodb/index.js";
import { Server } from "socket.io";

import userRoter from "./routes/students/route.js";
import messageRoter from "./routes/messages/route.js";
import chatRoter from "./routes/chats/route.js";
import loginRoute from "./routes/login/route.js";
import taskRoter from "./routes/tasks/route.js";

import { sendErrorMsg } from "./utils/index.js";
import { SocketService } from "./services/socketService.js";

const port = 8080;
const app = express();
//const server = http.createServer(app);
// const wss = new Server(server, {cors: {origin: "http://localhost:3000"}});
const wss = new Server(8000, { cors: { origin: "*" } });

SocketService.init(wss);

app.use(
  cors({
    origin: "http://localhost:3000",
    optionsSuccessStatus: 200,
    credentials: true,
  })
);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  session({
    secret: config().parsed.SESSION_SECRET,
    resave: false,
    saveUninitialized: true,
    store: MongoStore.create({
      collectionName: "sessions",
      mongoUrl: config().parsed.DB_SECRET,
      resave: false,
      saveUninitialized: false,
      autoRemove: "native",
    }),
    cookie: {
      expires: Date.now() + 1000 * 60 * 60 * 24,
      maxAge: 1000 * 60 * 60 * 24,
      httpOnly: true,
    },
  })
);

app.use(function auth(req, res, next) {
  //console.log("auth", req.session.userId, req.url);
  if (req.session.userId || req.url === "/login" || req.url === "/logout") {
    //console.log("auth", req.session.userId);
    next();
    //console.log("stop");
  } else if (req.url.startsWith("/socket.io")) {
    next();
  }else {
    //console.log("unathorize");
    sendErrorMsg(res, 401, "Unauthorized");
  }
});
app.use("/", loginRoute);
app.use("/students", userRoter);
app.use("/messages", messageRoter);
app.use("/tasks", taskRoter);
app.use("/chats", chatRoter);

app.listen(port, (err) => {
  if (err) {
    return console.log("Error: ", err);
  }
  console.log(`Server is listening on ${port}`);
});
