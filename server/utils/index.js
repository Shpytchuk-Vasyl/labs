export function sendErrorMsg(res, status, message) {
  res.statusText = message;
  res.status(status).send({ message: message });
  
  //res.status(status).end();
}

export async function tryCathc(res, action /*: () => any*/) {
  try {
    console.log("try", action + "");
    res.json(await action());
  } catch (err) {
    console.log(err);
    if (err instanceof MyError) {
      sendErrorMsg(res, err.code, err.message);
      console.log("send MyError:", err.code, err.message);
    } else sendErrorMsg(res, 500, "Something went wrong");
  }
}

export class MyError extends Error {
  code;

  constructor(message, code) {
    super(message);
    this.code = code; 
    this.name = this.constructor.name;
  }
}
