import { Schema, model } from "mongoose";
import User from "./userModel.js";
import Message from "./messageModel.js";

const chatSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  messages: [
    {
      type: Schema.Types.ObjectId,
      ref: Message,
    }
  ],
  users: [
    {
      type: Schema.Types.ObjectId,
      ref: User,
      required: true,
    }
  ],
}, {
  timestamps: true,
});

const Chat = model("chats", chatSchema);

export default Chat;
