import { Schema, model } from "mongoose";
import User from "./userModel.js"

const messageSchema = new Schema({
  chatId: {
    type: String,
    required: true,
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: User,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
    index: true,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
});

const Message = model("messages", messageSchema);

export default Message;
