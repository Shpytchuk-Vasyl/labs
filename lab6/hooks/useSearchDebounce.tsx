"use client";
import { useCallback, useEffect, useState } from "react";


export function useSearchDebounce(delay: number = 1000) {
  const [search, setSearch] = useState<string>("");
  const [searchQuery, setSearchQuer] = useState<string>("");

  const setSearchQuery = useCallback((newQuery: string) => {
    setSearchQuer((prev) => newQuery);
  }, []);

  useEffect(() => {
    const delayFn = setTimeout(() => setSearch(searchQuery), delay);
    return () => clearTimeout(delayFn);
  }, [searchQuery, delay]);

  return [search, setSearchQuery];
}
