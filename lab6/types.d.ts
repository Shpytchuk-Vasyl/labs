type User = {
  _id: string;
  firstName: string;
  lastName: string;
  group: string;
  gender: "F" | "M";
  birthday: string;
  status: boolean;
  unreadMessages?: UnreadMessages[];
  avatar?: string;
};

type UnreadMessages = {
  chatId: string;
  unread: number;
};


type Message = {
  _id: string;
  chatId: string;
  author: string | User;
  createdAt: string;
  text: string;
};

type Chat = {
  _id: string;
  name: string;
  messages: Message[];
  users: User[];
};

type Task = {
  _id: string;
  name: string;
  description?: string;
  deadline: string;
  stage: string;
}


