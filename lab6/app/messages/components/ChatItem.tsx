import React from "react";

type Props = {
  id: string;
  name: string;
  lastMessage: Message;
  unread: number;
  active: boolean;
  onClick: () => void;
};

const ChatItem = ({ id, lastMessage, name, unread, active, onClick }: Props) => {
  //console.log("ChatItem", id, lastMessage, name,"unread message", unread, active, onClick);
  
  return (
    <li
      className={
        active
          ? "list-group-item d-flex justify-content-between align-items-start bg-info "
          : "list-group-item d-flex justify-content-between align-items-start"
      }
      onClick={onClick}
    >
      <div className="ms-2 me-auto text-truncate ">
        <div className="fw-bold ">{name}</div>
        {lastMessage.text}
      </div>
      {unread > 0 && (
        <span className="badge text-bg-primary rounded-pill">{unread}</span>
      )}
    </li>
  );
};

export default ChatItem;
