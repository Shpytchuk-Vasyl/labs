"use client";
import React, { useState } from "react";
import { Modal } from "./Modal";
import {
  useDeleteStudentModal,
  useStudentModal,
} from "../contexts/ModalsContext";
import { useStudent } from "../contexts/StudentsContext";
import { toast } from "react-toastify";
interface Props {
  // isOpen: boolean;
  // title: string;
  // student?: Student;
  // setStudent: Dispatch<SetStateAction<Student | undefined>>;
}

const DeleteStudentModal = ({}: Props) => {
  const { isOpen, student: students, closeModal } = useDeleteStudentModal();
  const { removeStudent } = useStudent();

  const deleteStudent = async () => {
    students?.forEach(async (st) => {
      await deleteRequest(
        fetch(`${process.env.SERVER_URL}students/${st._id}`, {
          method: "DELETE",
          credentials: "include",
        }),
        () => removeStudent(st)
      );
    });
  };

  return (
    <Modal
      isOpen={isOpen}
      useHeader={false}
      title={"Delete student"}
      onClose={closeModal}
      body={
        <h2>
          Are you sure you want to delete student
          {(students && students.length > 1 ? "s " : " ") +
            (students
              ? students
                  .map((student) => student.firstName + " " + student.lastName)
                  .join(", ")
              : "") +
            "  ?"}
        </h2>
      }
      footer={
        <>
          <div className="d-flex flex-row-reverse  gap-3  ">
            <button
              className="btn btn-outline-dark p-2  object-fit-contain "
              id="form-ok"
              type="button"
              aria-label="ok"
              onClick={closeModal}
            >
              Cansel
            </button>
            <button
              className="btn btn-outline-dark p-2  object-fit-contain "
              id="form-save"
              type="submit"
              formNoValidate
              aria-label="save"
              onClick={deleteStudent}
            >
              Ok
            </button>
          </div>
        </>
      }
    />
  );

  async function deleteRequest(
    promise: Promise<Response>,
    callback: () => void
  ) {
    promise
      .then((res) => {
        if (res.status !== 200) throw res;
        toast.success("Student deleted", {
          autoClose: 2000,
          draggable: true,
          position: "bottom-right",
          closeOnClick: true,
        });
        
        callback();
        closeModal();
      })
      .catch(async (res) => {
        const err = await res.json();
        toast.error(err.message, {
          autoClose: 2000,
          draggable: true,
          position: "bottom-right",
          closeOnClick: true,
        });
      });
  }
};

export default DeleteStudentModal;
