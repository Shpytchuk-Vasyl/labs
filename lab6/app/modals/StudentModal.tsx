"use client";
import React, { useCallback, useState } from "react";
import { Modal } from "./Modal";
import { useStudentModal } from "../contexts/ModalsContext";
import { useStudent } from "../contexts/StudentsContext";
import { toast } from "react-toastify";
import { useAuth } from "../contexts/AuthContext";
interface Props {
  // isOpen: boolean;
  // title: string;
  // student?: Student;
  // setStudent: Dispatch<SetStateAction<Student | undefined>>;
}

const StudentModal = ({}: Props) => {
  const { isOpen, title, student, setStudent, closeModal } = useStudentModal();
  const { addStudent, editStudent } = useStudent();
  const [isSaving, setIsSaving] = useState<boolean>(false);
  const { I, setUser } = useAuth();

  const save = async () => {
    const form = document.getElementById("users-form") as HTMLFormElement;
    if (form.checkValidity() && !isSaving) {
      setIsSaving(true);
      const st = getStudentFromForm(new FormData(form));
      st.status = student?.status || false;
      if (student) {
        await sendRequest(
          fetch(`${process.env.SERVER_URL}students/${student._id}`, {
            headers: {
              "Content-Type": "application/json",
            },
            method: "PUT",
            credentials: "include",
            body: JSON.stringify(st),
          }),
          false
        );
      } else {
        await sendRequest(
          fetch(`${process.env.SERVER_URL}students`, {
            headers: {
              "Content-Type": "application/json",
            },
            method: "POST",
            credentials: "include",
            body: JSON.stringify(st),
          }),
          true
        );
      }
      setIsSaving(false);
    }
    form.classList.add("was-validated");
  };

  return (
    <Modal
      isOpen={isOpen}
      useHeader={false}
      title={title}
      onClose={closeModal}
      body={
        <form
          id="users-form"
          className=" p-3  bg-white border border-black rounded  needs-validation"
          onSubmit={save}
          noValidate
        >
          <div className="container-fluid">
            <div className="d-none">
              <input
                type="hidden"
                id="userId"
                name="userId"
                value={student?._id}
                aria-label="hidden"
              />
            </div>
            <div className="flex-column mb-3 ">
              <label htmlFor="group">Group</label>
              <input
                className="form-control"
                name="group"
                id="group"
                list="groups"
                value={student?.group}
                onChange={(e) =>
                  setStudent((prev) =>
                    prev ? { ...prev, group: e.target.value } : undefined
                  )
                }
                defaultValue="PZ-26"
                pattern="PZ-2[1-7]"
                required
              />

              <datalist id="groups">
                <option value="PZ-21">PZ-21</option>
                <option value="PZ-22">PZ-22</option>
                <option value="PZ-23">PZ-23</option>
                <option value="PZ-24">PZ-24</option>
                <option value="PZ-25">PZ-25</option>
                <option value="PZ-26">PZ-26</option>
                <option value="PZ-27">PZ-27</option>
              </datalist>
              <div className="invalid-feedback">Incorrect group</div>
            </div>
            <div className="form-floating mb-3 ">
              <input
                type="text"
                className="form-control"
                id="first-name"
                name="firstName"
                placeholder="first name"
                onChange={(e) =>
                  setStudent((prev) =>
                    prev ? { ...prev, firstName: e.target.value } : undefined
                  )
                }
                pattern="[A-ZА-ЯІЄЇ][a-z'а-яїіє]{1,}"
                value={student?.firstName}
                required
              />
              <label htmlFor="first-name">First name</label>
              <div className="invalid-feedback">Pleas, enter first name</div>
            </div>

            <div className="form-floating  mb-3 ">
              <input
                type="text"
                className="form-control"
                id="last-name"
                name="lastName"
                value={student?.lastName}
                onChange={(e) =>
                  setStudent((prev) =>
                    prev ? { ...prev, lastName: e.target.value } : undefined
                  )
                }
                pattern="[A-ZА-ЯІЄЇ][a-z'а-яїіє]{1,}"
                placeholder="Last name"
                required
              />
              <label htmlFor="last-name">Last name</label>
              <div className="invalid-feedback">Pleas, enter last name</div>
            </div>

            <div className=" mb-3 ">
              <label htmlFor="gender">Gender</label>
              <select
                className="form-control"
                id="gender"
                name="gender"
                onChange={(e) =>
                  setStudent((prev) =>
                    prev
                      ? { ...prev, gender: e.target.value as "F" | "M" }
                      : undefined
                  )
                }
                value={student?.gender}
                defaultValue="F"
              >
                <option value="F">Female</option>
                <option value="M">Male</option>
              </select>
            </div>

            <div className=" mb-3 ">
              <label htmlFor="date">Date</label>
              <input
                type="date"
                className="form-control"
                id="date"
                name="birthday"
                min="1954-01-01"
                max={
                  new Date().getFullYear() -
                  16 +
                  new Date().toISOString().split("T")[0].substring(4)
                }
                value={student?.birthday}
                onChange={(e) =>
                  setStudent((prev) =>
                    prev ? { ...prev, birthday: e.target.value } : undefined
                  )
                }
                defaultValue={new Date().toISOString().split("T")[0]}
                required
              />
              <div className="invalid-feedback">Pleas, enter correct date</div>
            </div>
          </div>
        </form>
      }
      footer={
        <div className="d-flex flex-row-reverse  gap-3  ">
          <button
            className="btn btn-outline-dark p-2  object-fit-contain "
            id="form-ok"
            type="button"
            aria-label="ok"
            onClick={closeModal}
          >
            ok
          </button>
          <button
            className="btn btn-outline-dark p-2  object-fit-contain "
            id="form-save"
            type="submit"
            //formNoValidate
            aria-label="save"
            formTarget="users-form"
            onClick={save}
          >
            Save
          </button>
        </div>
      }
    />
  );

  function getStudentFromForm(data: FormData): User {
    return {
      _id: data.get("userId") as string,
      firstName: data.get("firstName") as string,
      lastName: data.get("lastName") as string,
      group: data.get("group") as string,
      status: false,
      gender: data.get("gender") as "F" | "M",
      birthday: data.get("birthday")?.toString() as string,
    };
  }

  async function sendRequest(promise: Promise<Response>, isPost: boolean) {
    await promise
      .then(async (res) => {
        if (res.status != 200) throw res;
        return res.json();
      })
      .then((data: User) => {
        toast.success("Success", {
          autoClose: 2000,
          draggable: true,
          position: "bottom-right",
          closeOnClick: true,
        });

        if (isPost) {
          // console.log("save new student", data);
          addStudent(data);
        } else {
          //console.log("edit student", data);
          editStudent(data);
          if (data._id == I!._id)
            setUser({
              ...I!,
              firstName: data.firstName,
              lastName: data.lastName,
              birthday: data.birthday,
              group: data.group,
              gender: data.gender,
            });
        }
        closeModal();
      })
      .catch(async (res) => {
        const err = await res.json();
        toast.error(err.message, {
          autoClose: 2000,
          draggable: true,
          position: "bottom-right",
          closeOnClick: true,
        });
      });
  }
};

export default StudentModal;
