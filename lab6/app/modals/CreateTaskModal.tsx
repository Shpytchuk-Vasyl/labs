"use client";
import React, { use, useCallback, useEffect, useState } from "react";
import { Modal } from "./Modal";
import { toast } from "react-toastify";
import { useAuth } from "../contexts/AuthContext";
import { useCreateTask } from "../contexts/CreateTaskContext";
import { useTasks } from "../contexts/TasksContext";
import { useRouter } from "next/navigation";

const CreateTaskModal = () => {
  const { closeModal, isEditing, isOpen, setIsEdit, setTask, task } =
    useCreateTask();
  const { addTask, updateTask, stages } = useTasks();
  const { role } = useAuth();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [deadline, setDeadline] = useState("");
  const [stage, setStage] = useState("");
  const [isSaving, setIsSaving] = useState<boolean>(false);


  useEffect(() => {
    if (task) {
      setName(task.name);
      setDescription(task?.description || "");
      setDeadline(task.deadline);
      setStage(task.stage);
    }
  }, [task])

  const onCreateTask = () => {
    createRequest(
      fetch(`${process.env.SERVER_URL}tasks`, {
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        method: "POST",
        body: JSON.stringify({
          name: name,
          description: description,
          deadline: deadline,
          stage: stage,
        }),
      })
    );
  };

  const onEditTask = () => {
    createRequest(
      fetch(`${process.env.SERVER_URL}tasks`, {
        headers: {
          "Content-Type": "application/json",
        },
        credentials: "include",
        method: "PUT",
        body: JSON.stringify({
          id: task!._id,
          task: {
            name: name,
            description: description,
            deadline: deadline,
            stage: stage,
          },
        }),
      })
    );
  };

  const onClose = useCallback(() => {
    if (isEditing) {
      setIsEdit(false);
      setTask(undefined);
    }
    closeModal();
  }, [isEditing]);

  const save = async (e) => {
    e.preventDefault();
    e.stopPropagation();
    const form = document.getElementById("task-form") as HTMLFormElement;
    console.log("save", form.checkValidity(), isSaving);
    if (form.checkValidity() && !isSaving) {
      setIsSaving(true);
      if (isEditing) {
        onEditTask();
      } else {
        onCreateTask();
      }
      setIsSaving(false);
    }
    form.classList.add("was-validated");
    return false;
  };

  return (
    <Modal
      isOpen={isOpen}
      useHeader={false}
      onClose={onClose}
      title="Task"
      body={
        <form
          className=" p-3  bg-white border border-black rounded  needs-validation"
          onSubmit={save}
          action={"POST"}
          noValidate
          id="task-form"
        >
          <div className="container-fluid">
            <div className="d-none">
              <input type="hidden" value={task?._id} aria-label="hidden" />
            </div>
            <div className="flex-column mb-3 ">
              <label htmlFor="board">Board</label>
              <input
                className="form-control"
                name="board"
                id="board"
                list="stages"
                value={stage}
                onChange={(e) => setStage(e.target.value)}
                //disabled={role != "ADMIN"}
                required
              />
              <datalist id="stages">
                {stages.map((stage) => (
                  <option value={stage} key={stage}>
                    {stage}
                  </option>
                ))}
              </datalist>
              <div className="invalid-feedback">Incorrect board</div>
            </div>
            <div className="form-floating mb-3 ">
              <input
                type="text"
                className="form-control"
                id="task-name"
                name="name"
                placeholder="Name"
                onChange={(e) => setName(e.target.value)}
                value={name}
                //disabled={role != "ADMIN"}
                required
              />
              <label htmlFor="task-name">Name</label>
              <div className="invalid-feedback">Pleas, enter name</div>
            </div>

            <div className=" mb-3 ">
              <label htmlFor="date">Date</label>
              <input
                type="date"
                className="form-control"
                id="date"
                name="date"
                min={new Date().toISOString().split("T")[0]}
                value={deadline}
                onChange={(e) => setDeadline(e.target.value)}
                //disabled={role != "ADMIN"}
                required
              />
              <div className="invalid-feedback">Pleas, enter correct date</div>
            </div>

            <div className=" mb-3 ">
              <label htmlFor="description">Description</label>
              <textarea
                className="form-control"
                name="description"
                id="description"
                cols={3}
                rows={3}
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                //disabled={role != "ADMIN"}
              ></textarea>
            </div>
          </div>
        </form>
      }
      footer={
        <div className="container-fluid fw-bolder fs-6  ">
          {!isEditing ? (
            <button
              className="btn btn-outline-dark p-2 w-100 h-100"
              type="submit"
              aria-label="Create"
             form="task-form"
            >
              Create
            </button>
          ) : (
            <button
              className="btn btn-outline-dark p-2 w-100 h-100"
              type="submit"
              aria-label="Edit"
              form="task-form"
            >
              Edit
            </button>
          )}
        </div>
      }
    />
  );

  async function createRequest(promise: Promise<Response>) {
    promise
      .then((res) => {
        //console.log(res);
        if (res.status !== 200) throw res;
        return res.json();
      })
      .then((taskRes) => {
        toast.success("Success", {
          autoClose: 2000,
          draggable: true,
          position: "bottom-right",
          closeOnClick: true,
        });
        console.log("taskRes", taskRes);
        if (!isEditing) addTask(taskRes);
        else updateTask(taskRes);
        onClose();
      })
      .catch(async (res) => {
        const err = await res.json();
        toast.error(err.message, {
          autoClose: 2000,
          draggable: true,
          position: "bottom-right",
          closeOnClick: true,
        });
      });
  }
};

export default CreateTaskModal;
