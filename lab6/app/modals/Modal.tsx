"use client";

import { useRef } from "react";

type Props = {
  useHeader: boolean;
  header?: React.ReactNode;
  title?: string;
  body: React.ReactNode;
  footer: React.ReactNode;
  isOpen?: boolean;
  className?: string;
  onClose?: () => void;
};

export const Modal: React.FC<Props> = ({
  body,
  footer,
  useHeader,
  header,
  isOpen,
  title,
  className,
  onClose,
}) => {

  const contentRef = useRef<HTMLDivElement | null>(null);

  const handleOutsideClick = (e: React.MouseEvent<HTMLDivElement>) => {
    if (contentRef.current && !contentRef.current.contains(e.target as Node)) {
      onClose?.();
    }
  };

  if (!isOpen) return null;

  return (
    <div
      className={"modal d-block " + className}
      id="exampleModal"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
      onClick={handleOutsideClick}
    >
      <div className="modal-dialog  modal-dialog-centered">
        <div className="modal-content" ref={contentRef}>
          <div className="modal-header">
            {useHeader ? (
              header
            ) : (
              <>
                <h1 className="modal-title fs-5" id="exampleModalLabel">
                  {title}
                </h1>
                <button
                  type="button"
                  className="btn-close"
                  data-bs-dismiss="modal"
                  aria-label="Close"
                  onClick={onClose}
                ></button>
              </>
            )}
          </div>
          <div className="modal-body">{body}</div>
          <div className="modal-footer">{footer}</div>
        </div>
      </div>
    </div>
  );
};
