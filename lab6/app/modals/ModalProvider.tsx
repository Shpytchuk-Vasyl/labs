"use client";

import { useEffect, useState } from "react";
import StudentModal from "./StudentModal";
import {
  DeleteStudentProvider,
  StudentModalContextProvider,
  useStudentModal,
} from "../contexts/ModalsContext";
import DeleteStudentModal from "./DeleteStudentModal";
import { CreateChatProvider } from "../contexts/CreateChatContext";
import CreateTaskModal from "./CreateTaskModal";
import { CreateTaskProvider } from "../contexts/CreateTaskContext";

interface Props {
  children: React.ReactNode;
}

const ModalProviders = ({ children }: Props) => {
  const [isMounted, setIsMounted] = useState(false);
  useEffect(() => {
    setIsMounted(true);
  }, []);

  if (!isMounted) return null;

  return (
    <>
      <StudentModalContextProvider>
        <DeleteStudentProvider>
          <CreateChatProvider>
            <CreateTaskProvider>{children}</CreateTaskProvider>
          </CreateChatProvider>
        </DeleteStudentProvider>
      </StudentModalContextProvider>
    </>
  );
};

export default ModalProviders;
