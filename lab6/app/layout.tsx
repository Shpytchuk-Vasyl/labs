import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import "bootstrap/dist/css/bootstrap.min.css";
import InstalBootstrap from "./instalBootstrap";
import Header from "./components/Header";
import ModalProviders from "./modals/ModalProvider";
import { StudentsProvider } from "./contexts/StudentsContext";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { ChatsProvider } from "./contexts/ChatContext";
import { AuthProvider, useAuth } from "./contexts/AuthContext";
import { useRouter } from "next/navigation";
import { NotificationProvider } from "./contexts/NotificationContext";
import { SocketProvider } from "./contexts/SocketContext";
import { TasksProvider } from "./contexts/TasksContext";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Student CMS",
  description: "Simple student cms",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className + " without-scroll"}>
        <InstalBootstrap />
        <StudentsProvider>
          <AuthProvider>
            <NotificationProvider>
              <ChatsProvider>
                <TasksProvider>
                  <Header />
                  <ModalProviders>
                    <SocketProvider>{children}</SocketProvider>
                  </ModalProviders>
                </TasksProvider>
              </ChatsProvider>
            </NotificationProvider>
          </AuthProvider>
        </StudentsProvider>
        <ToastContainer />
      </body>
    </html>
  );
}
