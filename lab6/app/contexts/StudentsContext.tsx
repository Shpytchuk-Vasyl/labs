"use client";
import {
  ReactNode,
  createContext,
  useCallback,
  useContext,
  useState,
} from "react";

interface StudentsContextType {
  students: User[];
  addStudent: (student: User) => void;
  removeStudent: (student: User) => void;
  editStudent: (student: User) => void;
  setSudents: (students: User[]) => void;
}

const StudentsContext = createContext<StudentsContextType>({
  students: [],
  addStudent: (student: User) => {},
  removeStudent: (student: User) => {},
  editStudent: (student: User) => {},
  setSudents: (students: User[]) => {},
});

export const useStudent = () => useContext(StudentsContext);

interface Props {
  children: ReactNode;
}
export const StudentsProvider = ({ children }: Props) => {
  const [students, setStudents] = useState<User[]>([]);

  const addStudent = useCallback((student: User) => {
    setStudents((prev) => [...prev, student]);
  }, []);

  const removeStudent = useCallback((student: User) => {
    setStudents((prev) => prev.filter((s) => s._id !== student._id));
  }, []);

  const editStudent = useCallback((student: User) => {
    setStudents((prev) => prev.map((s) => (s._id === student._id ? student : s)));
  }, []);

  const setSudents = useCallback((students: User[]) => {
    setStudents((prev) => students);
  }, []);

  return (
    <StudentsContext.Provider
      value={{ students, addStudent, removeStudent, editStudent, setSudents }}
    >
      {children}
    </StudentsContext.Provider>
  );
};
