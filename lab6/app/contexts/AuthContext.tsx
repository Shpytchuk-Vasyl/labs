"use client";
import {
  ReactNode,
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";

type Role = "ADMIN" | "STUDENT" | undefined;

interface authtType {
  I: User | undefined;
  role: Role;
  setUser: (I: User | undefined) => void;
  setRole: (role: Role) => void;
}

const AuthContext = createContext<authtType>({
  I: undefined,
  role: undefined,
  setUser: (user: User | undefined) => {},
  setRole: (role: Role) => {},
});

export const useAuth = () => useContext(AuthContext);

interface Props {
  children: ReactNode;
}

export const AuthProvider = ({ children }: Props) => {
  const [I, setI] = useState<User | undefined>(undefined);
  const [role, setRol] = useState<Role>(undefined);

  const setUser = useCallback((I: User | undefined) => {
    setI(I);
  }, []);
  const setRole = useCallback((role: Role) => {
    setRol(role);
  }, []);

  useEffect(() => {
    if (!I) {
      const login = async () => {
        process.env.SERVER_URL = "http://localhost:8080/";
        fetch(`${process.env.SERVER_URL}login/`, {
          method: "GET",
          credentials: "include",
        })
          .then((res) => {
            if (res.status === 200) {
              return res.json();
            }
          })
          .then((data) => {
            if (data) {
              setI(data.user);
              setRol(data.role);
            }
          });
      };
      login();
    }
    return () => {
      if (I && I.unreadMessages) {
        fetch(`${process.env.SERVER_URL}save`, {
          method: "POST",
          credentials: "include",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(I.unreadMessages.filter((unread) => unread.unread > 0)),
        });
      }
    };
  }, [I]);

  return (
    <AuthContext.Provider value={{ I, role, setRole, setUser }}>
      {children}
    </AuthContext.Provider>
  );
};
