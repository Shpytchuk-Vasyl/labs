"use client";
import {
  ReactNode,
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { useAuth } from "./AuthContext";

interface NotificationContextType {
  tasks: Task[];
  messages: Message[];
  addMessage: (message: Message) => void;
  setMessages: (messages: Message[]) => void;
  removeMessage: (message: Message) => void;
  addTask: (task: Task) => void;
  removeTask: (id: string) => void;
  setTasks: (task: Task[]) => void;
  //ToDO: add tasks
}

const NotificationContext = createContext<NotificationContextType>({
  messages: [],
  addMessage: function (message: Message): void {},
  setMessages: function (messages: Message[]): void {},
  removeMessage: function (message: Message): void {},
  tasks: [],
  addTask: function (task: Task): void {},
  removeTask: function (id: string): void {},
  setTasks: function (task: Task[]): void {},
});

export const useNotification = () => useContext(NotificationContext);

interface Props {
  children: ReactNode;
}

export const NotificationProvider = ({ children }: Props) => {
  const { I, role, setUser } = useAuth();
  const [messages, setMessages] = useState<Message[]>([]);
  const addMessage = useCallback((message: Message) => {
    setMessages((prev) => {
      let index = prev.findIndex((m) => m.chatId == message.chatId);
      if (index != -1) {
        prev[index] = message;
      } else {
        prev.push(message);
      }
      return prev;
    });
  }, []);
  const removeMessage = useCallback((message: Message) => {
    setMessages((prev) => prev.filter((m) => m !== message));
  }, []);

  const set = useCallback((messages: Message[]) => setMessages(messages), []);

  useEffect(() => {
    if (I && I.unreadMessages) {
      // console.log("download unreadMessages", I.unreadMessages);
      I.unreadMessages
        .filter((unread) => unread.unread > 0)
        .forEach(async (unread) => {
          const data = await fetchMsg(unread.chatId);
          if (data && !messages.find((m) => m._id == data[0]._id)) {
            // console.log("load message to notification\n\n\n\n\n\nn\p", data);
            addMessage(data[0]);
          }
        });
    }
  }, [I]);

  const [tasks, setTask] = useState<Task[]>([]);
  const addTask = useCallback((task: Task) => {
    setTask((prev) => [...prev.filter((t) => t._id != task._id), task]);
  }, []);
  const removeTask = useCallback((id: string) => {
    setTask((prev) => prev.filter((t) => t._id != id));
  }, []);
  const setTasks = useCallback((tasks: Task[]) => {
    setTask((prev) => tasks);
  }, []);

  return (
    <NotificationContext.Provider
      value={{
        messages,
        addMessage,
        setMessages: set,
        removeMessage,
        tasks,
        addTask,
        removeTask,
        setTasks,
      }}
    >
      {children}
    </NotificationContext.Provider>
  );

  async function fetchMsg(chatId: string): Promise<Message[] | undefined> {
    const res = await fetch(
      `${process.env.SERVER_URL}messages/${chatId}?limit=1`,
      {
        method: "GET",
        credentials: "include",
      }
    );
    if (res.status === 200) {
      return res.json();
    }
    return undefined;
  }
};
