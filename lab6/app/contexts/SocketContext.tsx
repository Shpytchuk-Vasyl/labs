"use client";
import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { useAuth } from "./AuthContext";
import { Socket, io } from "socket.io-client";
import { useChats } from "./ChatContext";
import { useNotification } from "./NotificationContext";
import { useTasks } from "./TasksContext";
const ws = io("http://localhost:8000", { autoConnect: false });

interface SocketContextType {
  socket: Socket | undefined;
  currentChat: Chat | undefined;
  setChat: (chat: Chat | undefined) => void;
}

const SocketContext = createContext<SocketContextType>({
  socket: ws,
  currentChat: undefined,
  setChat: function (chat: Chat | undefined): void {},
});

export const useSocket = () => useContext(SocketContext);

interface Props {
  children: React.ReactNode;
}

export const SocketProvider = ({ children }: Props) => {
  const { I } = useAuth();
  const { reciveMessages, addMessageToChat, chats } = useChats();
  const { addTask, updateTask } = useTasks();
  const { addMessage, addTask: addTaskNotification } = useNotification();
  const [currentChat, setCurrentCha] = useState<Chat | undefined>(undefined);
  const setChat = useCallback((chat: Chat | undefined) => {
    console.log("set chat\n\n\\n\nn\n\n\nn", chat);
    setCurrentCha(chat);
  }, []);

  const onNewMessage = useCallback(
    (message: any) => {
      console.log("new message", message, currentChat);
      
      if (currentChat && message.chatId == currentChat._id) {
        addMessageToChat(message, currentChat);
        console.log("push message to chat", message);
      } else {
        console.log("message as notificatio", message);
        
        reciveMessages([message]);
        //@ts-ignore
        message?.users?.forEach((user) => {
          if (user._id == message.author) {
            message.author = user;
          }
        });
        addMessage(message);
      }
    },
    [currentChat, addMessageToChat, reciveMessages, addMessage]
  );

  const onNewTask = useCallback(
    (task: Task) => {
      addTask(task);
      addTaskNotification({ ...task, isNew: true } as Task);
    },
    [addTask]
  );

  const onUpdateTask = useCallback(
    (task: Task) => {
      updateTask(task);
      addTaskNotification({ ...task, isNew: false } as Task);
    },
    [updateTask]
  );

  const addListeners = useCallback(
    (ws: Socket) => {
      ws.on("new-msg", onNewMessage);
      ws.on("new-task", onNewTask);
      ws.on("update-task", onUpdateTask);
    },
    [onNewMessage, onNewTask, onUpdateTask]
  );

  const removeListeners = useCallback((ws: Socket) => {
    ws.removeListener("new-msg");
    ws.removeListener("new-task");
    ws.removeListener("update-task");
  }, []);

  useEffect(() => {
    if (I && !ws.connected) {
      ws.connect();
      ws.on("connect", () => {
        ws.emit("login", JSON.stringify(I));
        console.log("connected");
      });
      ws.on("disconnect", () => {
        console.log("disconnected");
      });
      ws.on("error", (error) => {
        console.log("error", error);
      });
      addListeners(ws);
    } else if (I) {
      removeListeners(ws);
      addListeners(ws);
    }
    return () => {
      removeListeners(ws);
    };
  }, [I, currentChat, chats, addListeners, removeListeners]);

  return (
    <SocketContext.Provider value={{ socket: ws, currentChat, setChat }}>
      {children}
    </SocketContext.Provider>
  );
};
