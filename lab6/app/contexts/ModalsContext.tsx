"use client";
import {
  Dispatch,
  SetStateAction,
  createContext,
  useContext,
  useState,
} from "react";
import StudentModal from "../modals/StudentModal";
import DeleteStudentModal from "../modals/DeleteStudentModal";

interface StudentsContextType {
  title: string;
  student?: User;
  isOpen: boolean;
  openModal: (student?: User) => void;
  setStudent: Dispatch<SetStateAction<User | undefined>>;
  closeModal: () => void;
}

const StudentModalContext = createContext<StudentsContextType>({
  title: "",
  isOpen: false,
  openModal: function (student?: User | undefined): void {},
  setStudent: function (value: SetStateAction<User | undefined>): void {},
  closeModal: function (): void {},
});

export const useStudentModal = () => useContext(StudentModalContext);

interface Props {
  children: React.ReactNode;
}

export const StudentModalContextProvider = ({ children }: Props) => {
  const [student, setStudent] = useState<User | undefined>();
  const [isOpen, setIsOpen] = useState(false);
  const [title, setTitle] = useState("");
  const openModal = function (student?: User | undefined): void {
    setIsOpen((prev) => true);
    setTitle((prev) => (student ? "Edit student" : "Add student"));
    setStudent((prev) => student);
  };

  const closeModal = function (): void {
    setIsOpen((prev) => false);
  };

  return (
    <StudentModalContext.Provider
      value={{ title, student, isOpen, openModal, setStudent, closeModal }}
    >
      <StudentModal />
      {children}
    </StudentModalContext.Provider>
  );
};

interface DeleteStudentsContextType {
  student?: User[];
  isOpen: boolean;
  openModal: (student: User[]) => void;
  closeModal: () => void;
}

const DelteStudentModalContext = createContext<DeleteStudentsContextType>({
  isOpen: false,
  openModal: function (student: User[]): void {},
  closeModal: function (): void {},
});

export const useDeleteStudentModal = () => useContext(DelteStudentModalContext);

interface Props {
  children: React.ReactNode;
}

export const DeleteStudentProvider = ({ children }: Props) => {
  const [student, setStudent] = useState<User[] | undefined>();
  const [isOpen, setIsOpen] = useState(false);
  const openModal = function (student: User[] | undefined): void {
    setIsOpen((prev) => true);
    setStudent((prev) => student);
  };

  const closeModal = function (): void {
    setIsOpen((prev) => false);
  };

  return (
    <DelteStudentModalContext.Provider
      value={{ isOpen, openModal, closeModal, student }}
    >
      <DeleteStudentModal />
      {children}
    </DelteStudentModalContext.Provider>
  );
};
