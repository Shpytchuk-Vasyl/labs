"use client";

import {
  ReactNode,
  createContext,
  useCallback,
  useContext,
  useState,
} from "react";

interface TaskContextType {
  stages: string[];
  tasks: Task[];
  addTask: (task: Task) => void;
  removeTask: (id: string) => void;
  updateTask: (task: Task) => void;
  setTasks: (tasks: Task[]) => void;
  setStages: (stages: string[]) => void;
}

const TasksContext = createContext<TaskContextType>({
  stages: [],
  tasks: [],
  addTask: () => {},
  removeTask: () => {},
  updateTask: () => {},
  setTasks: () => {},
  setStages: () => {},
});

export const useTasks = () => useContext(TasksContext);

interface Props {
  children: ReactNode;
}

export const TasksProvider = ({ children }: Props) => {
  const [tasks, setTask] = useState<Task[]>([]);
  const [stages, setStage] = useState<string[]>([]);
  const addTask = useCallback((task: Task) => {
    setTask((prev) => [...prev.filter((t) => t._id != task._id), task]);
  }, []);
  const removeTask = useCallback((id: string) => {
    setTask((prev) => prev.filter((t) => t._id != id));
  }, []);
  const updateTask = useCallback((task: Task) => {
    console.log("update task", task);
    setTask((prev) => {
      console.log("prev filter");
      return prev.filter((t) => t._id != task._id);
    });
    setTask((prev) => {
      console.log("prev add", prev);
      return [...prev, task];
    });
  }, []);
  const setTasks = useCallback((tasks: Task[]) => {
    setTask((prev) => tasks);
  }, []);
  const setStages = useCallback((stages: string[]) => {
    setStage((prev) => stages);
  }, []);

  return (
    <TasksContext.Provider
      value={{
        stages,
        tasks,
        addTask,
        removeTask,
        updateTask,
        setTasks,
        setStages,
      }}
    >
      {children}
    </TasksContext.Provider>
  );
};
