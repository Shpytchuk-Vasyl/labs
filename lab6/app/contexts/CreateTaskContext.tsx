"use client";
import {
  ReactNode,
  createContext,
  useCallback,
  useContext,
  useState,
} from "react";
import CreateTaskModal from "../modals/CreateTaskModal";

interface CreateTaskContextType {
  isEditing: boolean;
  task?: Task;
  isOpen: boolean;
  openModal: () => void;
  closeModal: () => void;
  setTask: (task: Task | undefined) => void;
  setIsEdit: (isEdit: boolean) => void;
}

const CreateTaskContext = createContext<CreateTaskContextType>({
  isEditing: false,
  task: undefined,
  isOpen: false,
  openModal: () => {},
  closeModal: () => {},
  setTask: (task: Task | undefined) => {},
  setIsEdit: (isEdit: boolean) => {},
});

export const useCreateTask = () => useContext(CreateTaskContext);

interface Props {
  children: ReactNode;
}
export const CreateTaskProvider = ({ children }: Props) => {
  const [isEditing, setIsEditing] = useState<boolean>(false);
  const [isOpen, setIsOpen] = useState(false);
  const [task, set] = useState<Task | undefined>(undefined);

  const openModal = useCallback(() => {
    setIsOpen(true);
  }, []);

  const closeModal = useCallback(() => {
    setIsOpen(false);
  }, []);

  const setIsEdit = useCallback((isEdit: boolean) => {
    setIsEditing(isEdit);
  }, []);

  const setTask = useCallback((task: Task | undefined) => {
    set((prev) => task);
  }, []);

  return (
    <CreateTaskContext.Provider
      value={{
        closeModal,
        isEditing,
        isOpen,
        openModal,
        setTask,
        setIsEdit,
        task,
      }}
    >
      <CreateTaskModal />
      {children}
    </CreateTaskContext.Provider>
  );
};
