"use client";
import Link from "next/link";
import Navbar from "./Navbar";
import { usePathname } from "next/navigation";
import Notification from "./Notification";
import SigninButtons from "./SigninButtons";
import { useAuth } from "../contexts/AuthContext";
import UserIcon from "./UserIcon";

type Props = {};

const Header = (props: Props) => {
  const path = usePathname();
  const {I} = useAuth();

  if(!I) return null;


  return (
    <header className="w-100 sticky-top" style={{ zIndex: 99 }}>
      <div className="d-flex justify-content-between bg-secondary pe-3 align-items-center ">
        <Navbar path={path} />
        <div className="d-flex justify-content-between gap-2  align-items-center">
          <Notification />
          <Link href="/" className="profile-container" aria-label="profile">
            <UserIcon
              url={
                I?.avatar ||
                "/user.jpg"
              }
            />
            {/* <div className="profile-photo">
              <svg
                className="w-6 h-6 text-gray-800 dark:text-white"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
              >
                <path
                  stroke="currentColor"
                  strokeWidth="2"
                  d="M7 17v1c0 .6.4 1 1 1h8c.6 0 1-.4 1-1v-1a3 3 0 0 0-3-3h-4a3 3 0 0 0-3 3Zm8-9a3 3 0 1 1-6 0 3 3 0 0 1 6 0Z"
                />
              </svg>
            </div> */}
          </Link>
          <SigninButtons />
        </div>
      </div>
    </header>
  );
};

export default Header;
