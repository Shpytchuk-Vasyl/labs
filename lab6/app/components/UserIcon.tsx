import Image from "next/image";
import React from "react";

type Props = {
  url: string;
};

const UserIcon = ({ url }: Props) => {
  return (
    <Image
      className="profile-photo  object-fit-contain"
      src={url}
      alt="user profile"
      width={40}
      height={40}
    ></Image>
  );
};

export default UserIcon;
