"use client";
import { useRouter } from "next/navigation";
import React, { use, useCallback } from "react";

type Props = {
  task: Task;
};

const TaskItem = ({ task}: Props) => {
  return (
    <div className="notification-content-msg">
      {/* @ts-ignore */}
      <span>{task?.isNew ? "New:" : "Update:"}</span>
      <div>
        <p>{task.name}</p>
      </div>
    </div>
  );
};

export default TaskItem;
