"use client";
import { useCallback, useState } from "react";
import TableRows from "./components/TableRows";
import {
  useDeleteStudentModal,
  useStudentModal,
} from "./contexts/ModalsContext";
import { useAuth } from "./contexts/AuthContext";
import { useRouter } from "next/navigation";

export default function Home() {
  const { I, role } = useAuth();
  const router = useRouter();

  const { openModal: openEditModal } = useStudentModal();
  const { openModal: removeStudent } = useDeleteStudentModal();
  const [selectAll, setSelectAll] = useState(false);

  const addStudent = useCallback(() => {
    openEditModal();
  }, []);

  const editStudent = (student: User) => {
    openEditModal(student);
  };

  if (!I) {
    //console.log("no user");
    router.push("/login");
    return null;
  }

  return (
    <main className="container-fluid mt-4 ">
      <div className="d-flex justify-content-between p-2">
        <h2 className="">Students</h2>
        {role == "ADMIN" && (
          <button
            className="btn btn-outline-dark py-0"
            onClick={addStudent}
            aria-label="Add new student"
          >
            <svg
              className="align-baseline inverse-color"
              xmlns=" http://www.w3.org/2000/svg"
              viewBox="0 0 448 512"
              height="1rem"
              color="currentColor"
            >
              <path
                strokeWidth="1"
                stroke="currentColor"
                style={{ fill: "currentColor" }}
                d="M416 208H272V64c0-17.7-14.3-32-32-32h-32c-17.7 0-32 14.3-32 32v144H32c-17.7 0-32 14.3-32 32v32c0 17.7 14.3 32 32 32h144v144c0 17.7 14.3 32 32 32h32c17.7 0 32-14.3 32-32V304h144c17.7 0 32-14.3 32-32v-32c0-17.7-14.3-32-32-32z"
              />
            </svg>
          </button>
        )}
      </div>
      <div className="container-fluid p-2  overflow-x-auto">
        <table className="table table-bordered  table-hover text-center align-middle ">
          <thead className="table-light">
            <tr>
              <th>
                <input
                  type={"checkbox"}
                  className="delete-checkbox"
                  aria-label="Delete all checkbox"
                  onChange={(e) => {
                    setSelectAll(e.target.checked);
                  }}
                  name="delete-checkbox"
                />
              </th>
              <th>Group</th>
              <th>Name</th>
              <th>Gender</th>
              <th>Birthday</th>
              <th>Status</th>
              <th>Option</th>
            </tr>
          </thead>
          <tbody id="table-body" className="table-group-divider">
            <TableRows
              selectAll={selectAll}
              onDelete={removeStudent}
              onEdit={editStudent}
            />
          </tbody>
        </table>
      </div>
    </main>
  );
}
