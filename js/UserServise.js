class UserService {
  row = null;

  showUserForm(user, editRow) {
    this.row = editRow;
    $("#users-form")[0].style = "display: fixed";
    $("#form-show-error").addClass("d-none");

    let inputs = $("#users-form input, #users-form select");
    inputs[0].value = user.id;
    inputs[1].value = user.group;
    inputs[2].value = user.first;
    inputs[3].value = user.last;
    inputs[4].value = user.gender;
    inputs[5].value = user.birthday.split(".").reverse().join("-");
  }

  closeUserForm() {
    $("#users-form")[0].style = "display: none";
  }

  addNewUserToTable(user) {
    $("#table-body").append(this.getRow(user));
  }

  editUser(user) {
    this.row.replaceWith(this.getRow(user));
    this.row = undefined;
  }

  deleteUser(row) {
    row.remove();
  }

  validateUserData(user) {
    if (
      !Array.from($("#groups option"))
        .map((e) => e.value)
        .includes(user.group)
    ) {
      $("#group").addClass("is-invalid");
      $("#group").removeClass("is-valid");
      return null;
    }

    const [day, month, year] = user.birthday ? user.birthday.split(".") : "";
    const userDate = new Date(`${year}-${month}-${day}`);
    const today = new Date();

    if (
      isNaN(userDate.getTime()) ||
      today.getFullYear() - userDate.getFullYear() > 70 ||
      today.getFullYear() - userDate.getFullYear() < 16
    ) {
      $("#date").addClass("is-invalid");
      $("#date").removeClass("is-valid");
      return null;
    } else {
      $("#date").addClass("is-valid");
      $("#date").removeClass("is-invalid");
    }
    return user;
  }

  getRow(user) {
    console.log(user);
    let innerHTML = `
     <td>
          <input type="checkbox" class="delete-checkbox" aria-label="Delete checkbox" value="${
            user.id
          }" name="delete-checkbox">
      </td>
      <td>${user.group}</td>
      <td>
        <span>${user.first}</span>
        <span> ${user.last}</span>
      </td>
      <td>${user.gender}</td>
      <td>${user.birthday.split("-").reverse().join(".")}</td>
      <td>
          <span class="rounded-circle ${
            user.status ? "text-bg-success" : "text-bg-secondary"
          } d-inline-block"
              style="width: 1.5rem;  height: 1.5em;"></span>
      </td>
      <td>
          <button class="btn btn-outline-dark" onclick="showEdittingUserForm(this)"
              aria-label="edit user">
              <svg xmlns="http://www.w3.org/2000/svg" class="align-baseline  " height="1rem"
                  viewBox="0 0 512 512">
                  <path
                      d="M471.6 21.7c-21.9-21.9-57.3-21.9-79.2 0L362.3 51.7l97.9 97.9 30.1-30.1c21.9-21.9 21.9-57.3 0-79.2L471.6 21.7zm-299.2 220c-6.1 6.1-10.8 13.6-13.5 21.9l-29.6 88.8c-2.9 8.6-.6 18.1 5.8 24.6s15.9 8.7 24.6 5.8l88.8-29.6c8.2-2.7 15.7-7.4 21.9-13.5L437.7 172.3 339.7 74.3 172.4 241.7zM96 64C43 64 0 107 0 160V416c0 53 43 96 96 96H352c53 0 96-43 96-96V320c0-17.7-14.3-32-32-32s-32 14.3-32 32v96c0 17.7-14.3 32-32 32H96c-17.7 0-32-14.3-32-32V160c0-17.7 14.3-32 32-32h96c17.7 0 32-14.3 32-32s-14.3-32-32-32H96z" />
              </svg>
          </button>
          <button class="  btn btn-outline-dark" data-bs-toggle="modal"
              data-bs-target="#deleteUserModal" aria-label="delete user"
              onclick="deleteTableRow(this)">
              <!-- <i class="fa fa-close" style="font-size:24px"></i> -->
              <svg xmlns="http://www.w3.org/2000/svg" class="align-baseline  " height="1rem"
                  viewBox="0 0 352 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2024 Fonticons, Inc.-->
                  <path
                      d="M242.7 256l100.1-100.1c12.3-12.3 12.3-32.2 0-44.5l-22.2-22.2c-12.3-12.3-32.2-12.3-44.5 0L176 189.3 75.9 89.2c-12.3-12.3-32.2-12.3-44.5 0L9.2 111.5c-12.3 12.3-12.3 32.2 0 44.5L109.3 256 9.2 356.1c-12.3 12.3-12.3 32.2 0 44.5l22.2 22.2c12.3 12.3 32.2 12.3 44.5 0L176 322.7l100.1 100.1c12.3 12.3 32.2 12.3 44.5 0l22.2-22.2c12.3-12.3 12.3-32.2 0-44.5L242.7 256z" />
              </svg>
          </button>
      </td>
    `;

    const newRow = document.createElement("tr");
    newRow.innerHTML = innerHTML;
    return newRow;
  }
}

class User {
  id;
  group;
  first;
  last;
  gender;
  birthday;
  status;

  constructor(array) {
    this.id = array[0];
    this.group = array[1];
    this.first = array[2];
    this.last = array[3];
    this.gender = array[4];
    this.birthday = array[5];
    this.status = array[6];
  }
}

const userService = new UserService();

function showAddingForm() {
  const date = new Date();
  const user = new User([
    "",
    "PZ-26",
    "",
    "",
    "M",
    "dd.mm.yyyy"
      .replace("mm", (date.getMonth() + 1).toString().padStart(2, "0"))
      .replace("yyyy", date.getFullYear().toString().padStart(4, "0"))
      .replace("dd", date.getDate().toString().padStart(2, "0")),
    ,
    false,
  ]);
  userService.showUserForm(user);
  $("#form-save")[0].textContent = "create";
  $("#form-header-text")[0].textContent = "Add student";
}

function closeUserForm() {
  userService.closeUserForm();
}

function showEdittingUserForm(button) {
  let row = button.parentNode.parentNode;
  let user = new User([
    row.children[0].children[0].value,
    row.children[1].textContent,
    row.children[2].children[0].textContent,
    row.children[2].children[1].textContent,
    row.children[3].textContent,
    row.children[4].textContent.split(".").reverse().join("-"),
    false,
  ]);
  userService.showUserForm(user, row);
  $("#form-save")[0].textContent = "save";
  $("#form-header-text")[0].textContent = "Edit student";
}

function saveUser() {
  const user = userService.validateUserData(
    new User(
      Array.from($("#users-form input, #users-form select")).map((e) => e.value)
    )
  );
  if (user) {
    $.ajax({
      url: "http://localhost:3000/Labs/ProgramingInInternetLabs/lab2/php/saveUser.php",
      type: "POST",
      dataType: "json",
      data: JSON.stringify({
        id: userService.row ? user.id : -1,
        group: user.group,
        firstName: user.first,
        lastName: user.last,
        gender: user.gender,
        birthday: user.birthday,
      }),
      success: function (response) {
        if (userService.row) userService.editUser(user);
        else userService.addNewUserToTable(response.message);
        userService.closeUserForm();
        const toast = $("#liveToast")[0];
        toast.classList.remove("bg-danger");
        toast.classList.add("bg-success");
        toast.children[1].innerText = "Succsess";
        bootstrap.Toast.getOrCreateInstance(toast).show();
      },
      error: function (error) {
        $("#form-show-error").removeClass("d-none");
        $("#form-show-error")[0].children[0].innerText =
          error?.responseJSON?.message || "Something went wrong";
      },
    });
  }
  return true;
}

function deleteTableRow(button) {
  $("#modalUserName")[0].innerHTML =
    "Are you sure you want delete user " +
    button.parentNode.parentNode.children[2].innerHTML +
    " ?";
  $("#modalDeleteOkButton").click(() => {
    $.ajax({
      url: "http://localhost:3000/Labs/ProgramingInInternetLabs/lab2/php/deleteUser.php",
      type: "POST",
      dataType: "json",
      data: JSON.stringify({
        id: button.parentNode.parentNode.children[0].children[0].value,
      }),
      success: function (response) {
        userService.deleteUser(button.parentNode.parentNode);
        const toast = $("#liveToast")[0];
        toast.classList.remove("bg-danger");
        toast.classList.add("bg-success");
        toast.children[1].innerText = response.message;
        bootstrap.Toast.getOrCreateInstance(toast).show();
      },
      error: function (error) {
        const toast = $("#liveToast")[0];
        toast.classList.add("bg-danger");
        toast.classList.remove("bg-success");
        toast.children[1].innerText =
          error.responseJSON?.message || "Something went wrong";
        bootstrap.Toast.getOrCreateInstance(toast).show();
      },
    });
  });
}

function selectAllInTable(checkbox) {
  Array.from($(".delete-checkbox")).forEach((element) => {
    element.checked = checkbox.checked;
  });
}

(() => {
  $.ajax({
    url: "http://localhost:3000/Labs/ProgramingInInternetLabs/lab2/php/getAllUsers.php",
    type: "GET",
    dataType: "json",
    success: function (response) {
      response.forEach((userJson) => {
        const user = JSON.parse(userJson);
        user.status = user.status === "1";
        userService.addNewUserToTable(user);
      });
    },
    error: function (error) {
      const toast = $("#liveToast")[0];
      toast.classList.add("bg-danger");
      toast.classList.remove("bg-success");
      toast.children[1].innerText =
        error.responseJSON?.message || "Something went wrong";
      bootstrap.Toast.getOrCreateInstance(toast).show();
    },
  });
})();

(() => {
  "use strict";
  const forms = document.querySelectorAll(".needs-validation");

  Array.from(forms).forEach((form) => {
    form.addEventListener(
      "submit",
      (event) => {
        event.preventDefault();
        event.stopPropagation();
        if (form.checkValidity() === true) {
          saveUser();
        }

        form.classList.add("was-validated");
      },
      false
    );
  });
})();
