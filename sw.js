// const cacheName = 'MyFancyCacheName_v1';

// self.addEventListener('install', (event) => {
//   event.waitUntil(caches.open(cacheName)/*.then((cache) => cache.addAll(
//     [
//       "index.html",
//       "css/header.css",
//       "css/index.css",
//       "js/UserService.js",
//       "assets/"
//     ]
//   ))*/);
// });


// self.addEventListener('fetch', (event) => {
//     event.respondWith(caches.open(cacheName).then((cache) => {
//       return cache.match(event.request.url).then((cachedResponse) => {
//         if (cachedResponse) {
//           return cachedResponse;
//         }


//         return fetch(event.request).then((fetchedResponse) => {    
//           cache.put(event.request, fetchedResponse.clone());
//           return fetchedResponse;
//         });
//       });
//     }));
// });