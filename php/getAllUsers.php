<?php
include './database.php';

header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: *");  
header("Access-Control-Allow-Headers: Content-Type");  

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    http_response_code(200);
error_log("GET ALL USERS");
    $response = getAllUserFromDB();
    error_log(json_encode($response));
    echo json_encode($response);
} else {
    http_response_code(405);
    $response = ['message' => 'Invalid request method.'];
    echo json_encode($response);
}
?>