<?php
include './database.php';

header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: *");  
header("Access-Control-Allow-Headers: Content-Type");  

$allowedGroups = ['PZ-21', 'PZ-22', 'PZ-23', 'PZ-24', 'PZ-25', 'PZ-26', 'PZ-27'];

function dataIsNotValid(&$response, $data) {
    global $allowedGroups;
    try {
        if (in_array(null, $data)) {
            $response = ['message' => 'All fields should be present'];
        }  else if(!validateName($data['first'])) {
            $response = ['message' => 'Invalid first name'];
        } else if(!validateName($data['last'])) {
            $response = ['message' => 'Invalid last name'];
        } elseif (!in_array($data['group'],  $allowedGroups)) {
            $response = ['message' => 'Incorrect group'];
        } elseif (!validateBirthday($data['birthday'])) {
            $response = ['message' => 'Invalid birthday format or age'];
        } else if(strtolower($data['gender']) !== 'm' && strtolower($data['gender']) !== 'f') {
            $response = ['message' => 'Gender should be either "M" or "F"'];
        } else {
            return false;
        }
   } catch (Exception $e) {
        error_log($e);
        $response = ['message' => 'Something went wrong'];
   }
    return true;
}

function validateName($name) {
    $pattern = '/[^\p{L}\s]/u';
    return strlen($name) >= 2 
    && strlen($name) <= 30 
    && preg_match($pattern, $name) === 0
    && startsWithUpper($name);
}

function startsWithUpper($str) {
    $chr = mb_substr ($str, 0, 1, "UTF-8");
    return mb_strtolower($chr, "UTF-8") != $chr 
    && mb_strtolower(mb_substr($str, 1), "UTF-8") === mb_substr($str, 1);
}

function validateBirthday($birthday) {
    try {  
        $age = (new DateTime())->diff(new DateTime($birthday))->y;
        return ($age >= 16 && $age <= 70);
    } catch (Exception $e) {
        return false;
    }
}


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    http_response_code(200);
    $jsonData = json_decode(file_get_contents('php://input'), true);

    $data = [
        'id'       => isset($jsonData['id']) ? $jsonData['id'] : -1,
        'group'    => isset($jsonData['group']) ? $jsonData['group'] : null,
        'first'    => isset($jsonData['firstName']) ? $jsonData['firstName'] : null,
        'last'     => isset($jsonData['lastName']) ? $jsonData['lastName'] : null,
        'gender'   => isset($jsonData['gender']) ? $jsonData['gender'] : null,
        'birthday' => isset($jsonData['birthday']) ? $jsonData['birthday'] : null
    ];
    foreach ($data as $key => $value) {
        error_log("$key: $value");
    }

    $response = [];

    if(dataIsNotValid($response, $data)) {
        http_response_code(400);
    } else if(!saveUserInDB($data)) {
        http_response_code(501);
        $response = ['message' => 'Unable to perform the operation'];
    } else {
        error_log("User saved successfully");
         $response = ['message' => $data];
    }
    echo json_encode($response);
} else {
    http_response_code(405);
    $response = ['message' => 'Invalid request method.'];
    echo json_encode($response);
}
?>