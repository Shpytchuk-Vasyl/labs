<?php
$servername = ("localhost");
$username = "root";
$password = "password";
$dbname = "cms";
phpinfo();



try {
    $conn = mysqli_connect($servername, $username, $password, $dbname) or
        error_log("Connection failed: " . $conn->connect_error);

} catch (Exception $e) {
    error_log($e);
    $conn = null;
}


function saveUserInDB(&$userData) {
    global $conn;
        try {
            if ($userData['id'] == -1) {
                $sql = "INSERT INTO students(`group`, `first`, `last`, gender, birthday) VALUES (?, ?, ?, ?, ?)";
                $stmt = $conn->prepare($sql);
                $gender = strtoupper($userData['gender']); 
                $stmt->bind_param("sssss", $userData['group'], $userData['first'], $userData['last'], $gender, $userData['birthday']); 
                $stmt->execute();
                error_log("New records created successfully");
                $userData['id'] = $conn->insert_id;
                return true ;
        
            } else {
                $sql = "UPDATE students SET `group` = ?, `first` = ?, `last` = ?, gender = ?, birthday = ? WHERE id = ?";
                $stmt = $conn->prepare($sql);
                $gender = strtoupper($userData['gender']);
                $stmt->bind_param("sssssi", $userData['group'], $userData['first'], $userData['last'], $gender, $userData['birthday'], $userData['id']); 
                return $stmt->execute();
           
            }
        
    } catch (Exception $e) {
        error_log($e);
        return false;
    }
}

function deleteUserInDB($id) {
    global $conn;
    try {
        $sql = "DELETE FROM students WHERE id = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("i", $id);
        return $stmt->execute();
    } catch (Exception $e) {
        error_log($e);
        return false;
    }
}


function getAllUserFromDB(){
    global $conn;
    $sql = "SELECT * FROM students";
    $result = $conn->query($sql);
    $response = [];
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $response[] = json_encode($row);
        }
    }
    return $response;
}

function isNotUserPresent($userData) {
    global $conn;
    try {
        $sql = "SELECT * FROM students WHERE `first` = ? AND `last` = ? AND `birthday` = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sss", $userData['first'], $userData['last'], $userData['birthday']);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->num_rows > 0;
    } catch (Exception $e) {
        error_log($e);
        return false;
    }
}

/* 
CREATE TABLE `cms`.`students` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `first` VARCHAR(45) NOT NULL,
  `last` VARCHAR(45) NOT NULL,
  `gender` ENUM("F", "M") NOT NULL,
  `birthday` DATE NOT NULL,
  `status` TINYINT NOT NULL DEFAULT 0,
  `group` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE);

*/

?>
