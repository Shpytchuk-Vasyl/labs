<?php
include './database.php';

header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: *");  
header("Access-Control-Allow-Headers: Content-Type");  

function dataIsNotValid(&$response, $data) {
    try {
        if (in_array(null, $data)) {
            $response = ['message' => 'All fields should be present'];
         } else if (!validateBirthday($data['pass'])) {
            $response = ['message' => 'Invalid password'];
        }  else if(!validateName($data['first'])) {
            $response = ['message' => 'Invalid first name'];
        } else if(!validateName($data['last'])) {
            $response = ['message' => 'Invalid last name'];
        } else {
            return false;
        }
   } catch (Exception $e) {
        error_log($e);
        $response = ['message' => 'Something went wrong'];
   }
    return true;
}

function validateBirthday($birthday) {
    $age = (new DateTime())->diff(new DateTime($birthday))->y;
    return ($age >= 16 && $age <= 70);
}


function validateName($name) {
    $pattern = '/[^\p{L}\s]/u';
    return strlen($name) >= 2 
    && strlen($name) <= 30 
    && preg_match($pattern, $name) === 0
    && startsWithUpper($name);
}


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    http_response_code(200);
    $jsonData = json_decode(file_get_contents('php://input'), true);

    $data = [
        'first'    => isset($jsonData['first']) ? $jsonData['first'] : null,
        'last'     => isset($jsonData['last']) ? $jsonData['last'] : null,
        'pass'   => isset($jsonData['pass']) ? $jsonData['pass'] : null,  
    ];
    foreach ($data as $key => $value) {
        error_log("$key: $value");
    }

    $response = [];

    if(dataIsNotValid($response, $data)) {
        http_response_code(401);
    } else if(isNotUserPresent($data)) {
        http_response_code(401);
        $response = ['message' => 'Incorrect login or password'];
    } else {
        error_log("Login successfully");
         $response = ['message' => $data];
    }
    echo json_encode($response);
} else {
    http_response_code(405);
    $response = ['message' => 'Invalid request method.'];
    echo json_encode($response);
}
?>